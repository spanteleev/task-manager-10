package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void remove(Project task);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    void clear();

}
