package ru.tsc.panteleev.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

}
