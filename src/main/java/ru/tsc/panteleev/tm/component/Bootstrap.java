package ru.tsc.panteleev.tm.component;

import ru.tsc.panteleev.tm.api.controller.ICommandController;
import ru.tsc.panteleev.tm.api.controller.IProjectController;
import ru.tsc.panteleev.tm.api.controller.ITaskController;
import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.ICommandService;
import ru.tsc.panteleev.tm.api.service.IProjectService;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;
import ru.tsc.panteleev.tm.controller.CommandController;
import ru.tsc.panteleev.tm.controller.ProjectController;
import ru.tsc.panteleev.tm.controller.TaskController;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.service.CommandService;
import ru.tsc.panteleev.tm.service.ProjectService;
import ru.tsc.panteleev.tm.service.TaskService;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        commandController.showWelcome();
        while (true) {
            System.out.println("Enter command:");
            String command = TerminalUtil.nextLine();
            runWithCommand(command);
        }
    }

    public void runWithCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public boolean runWithArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        final String command = args[0];
        if (command == null || command.isEmpty()) return false;
        runWithArgument(command);
        return true;
    }

    public void runWithArgument(String command) {
        switch (command) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument(command);
                break;
        }
    }

    public static void exit() {
        System.exit(0);
    }

}
